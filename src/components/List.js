import React from "react";
import logo from "../images/logo-icon.png";
import { Link } from "react-router-dom";
import axios from "axios";
import ListForm from "./ListForm";

export default class List extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state = {
      locationData: [],
      isDataLoaded: false,
      value: "Bangalore",
    };
  }

  async componentDidMount() {
    const response = await axios.post(
      "https://api.robostack.ai/external/api/ea54c3b7-3392-43de-9ca0-1b985c3ad98e/cities/",
      {},
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "x-api-key": "7507bdce-65bc-411c-888b-8454988e171b",
          "x-api-secret": "a79d656d-9c39-4f67-accf-b78c2acbd4b4",
        },
      }
    );
    this.setState({
      locationData: response.data.results,
      isDataLoaded: true,
    });
  }

  handleOnChange = (event) => {
    const data = event.target.value;
    this.setState({
      value: data,
    });
  };
  render() {
    if (this.state.isDataLoaded) {
      return (
        <>
          <header>
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-2 col-3 m-auto m-md-0 p-md-2">
                  <img src={logo} alt="logo" className="img-fluid logo" />
                </div>
                <div className="col-md-10 text-end d-md-block d-none">
                  <div className="d-md-flex flex-row justify-content-end mt-1">
                    <div className="p-2">
                      <Link className="link">Download App</Link>
                    </div>
                    <div className="p-2">
                      <Link className="link" to={`/show`}>Show Property</Link>
                    </div>
                    <div className="p-2">
                      <Link className="link">
                        <button className="btn bg-white px-3 py-0">
                          <span
                            className="btn-text px-1"
                            style={{ color: "blue" }}
                          >
                            List property
                          </span>
                          <span className="w-auto bg-danger free px-1">
                            Free
                          </span>
                        </button>
                      </Link>
                    </div>
                    <div className="p-2">
                      <Link className="link">Saved</Link>
                    </div>
                    <div className="p-2">
                      <Link className="link">News</Link>
                    </div>
                    <div className="p-2">
                      <Link className="link" to={`/signup`}>
                        <i class="far fa-user"></i>Login
                      </Link>
                    </div>
                    <div className="p-2">
                      <Link className="link">
                        <i class="fas fa-bars"></i>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container text-center mt-0 p-lg-3">
              <div className="row m-auto">
                <div className="col">
                  <p className="display-6 text-white">
                    Properties to sell in <strong>Bangaluru</strong>
                  </p>
                  <h5>
                    <span className="w-auto p-lg-5 p-md-1 mb-lg-4 text-white">
                      Yahan Search Khatam Karo
                    </span>
                  </h5>
                </div>
              </div>
            </div>
            <div className="container text-center p-3 mt-lg-3">
              <div className="row">
                <div className="col">
                  <div className="">
                    <select
                      name="location"
                      id="location"
                      defaultValue={this.state.value}
                      className="px-2 py-3 round w-50"
                      onChange={this.handleOnChange}
                    >
                      <option value="Bangalore">Bangalore</option>
                      <option value="Mumbai">Mumbai</option>
                      <option value="Patna">Patna</option>
                      <option value="Pune">Pune</option>
                      <option value="Chennai">Chennai</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </header>
          <ListForm />
        </>
      );
    } else {
      return <div className="loader"></div>;
    }
  }
}
