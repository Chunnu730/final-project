import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import amenities from "../images/amenities.png";

export default class Properties extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state = {
      propertiesData: [],
      locationData: [],
      isDataLoaded: false,
    };
  }

  async componentDidMount() {
    const response = await axios.post(
      "https://api.robostack.ai/external/api/ea54c3b7-3392-43de-9ca0-1b985c3ad98e/properties/",
      {},
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "x-api-key": "7507bdce-65bc-411c-888b-8454988e171b",
          "x-api-secret": "a79d656d-9c39-4f67-accf-b78c2acbd4b4",
        },
      }
    );
    const location = await axios.post(
      "https://api.robostack.ai/external/api/ea54c3b7-3392-43de-9ca0-1b985c3ad98e/cities/",
      {},
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "x-api-key": "7507bdce-65bc-411c-888b-8454988e171b",
          "x-api-secret": "a79d656d-9c39-4f67-accf-b78c2acbd4b4",
        },
      }
    );

    this.setState({
      locationData: location.data.results,
      propertiesData: response.data.results,
      isDataLoaded: true,
    });
  }
  getValue(locationData, propertiesData, nameValue) {
    const locationId = locationData.find(
      (element) => this.props.location === element.name
    ).id;

    const result = propertiesData.find(
      (element) => element.location_id === locationId
    )[nameValue];
    return result;
  }

  render() {
    const { locationData, propertiesData, isDataLoaded } = this.state;
    if (!isDataLoaded) {
      return<div className="loader"></div>
    }
      return (
        <>
          <div className="container-fluid bg-prop py-5">
            <div className="container ">
              <figure>
                <blockquote className="blockquote">
                  <h3>
                    In <strong>Spotlight</strong>
                  </h3>
                </blockquote>
                <figcaption className="mb-3">
                  Find your best place to live with us
                </figcaption>
              </figure>
            </div>
            <div className="container transform-property">
              <div className="row w-100  border border-shadow">
                <div className="col px-0">
                  <Link to={`/properties/details/${this.props.location}`}>
                    <img
                      src={this.getValue(locationData, propertiesData, "image")}
                      alt=""
                      className="img-fluid img p-0"
                    />
                  </Link>
                </div>
                <div className="col bg-white py-2">
                  <div className="row">
                    <div className="col">
                      <figure>
                        <blockquote className="blockquote">
                          <h5>
                            {this.getValue(
                              locationData,
                              propertiesData,
                              "name"
                            )}
                          </h5>
                        </blockquote>
                        <figcaption>
                          <div>
                            <small style={{ marginTop: "-17px" }}>
                              {this.getValue(
                                locationData,
                                propertiesData,
                                "address"
                              )}
                            </small>
                          </div>
                          <div style={{ color: "blueviolet" }}>
                            <strong>
                              <span className="lead">$</span>
                              {this.getValue(
                                locationData,
                                propertiesData,
                                "rate"
                              )}
                            </strong>
                          </div>
                        </figcaption>
                      </figure>
                    </div>
                    <div className="col">
                      <button className="btn contact bg-white py-1 px-4">
                        Contact
                      </button>
                    </div>
                  </div>
                  <div className="row">
                    <div col>
                      <h5>Amenities</h5>
                    </div>
                  </div>
                  <div className="row row-col-5">
                    <div className="col">
                       <img src={amenities} alt=""  className="img-fluid" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      );
    }
}
