import React from "react";
import Modal from "./Modal";
export default class ListForm extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state={
      name:"",
      cost:"",
      area:"",
      address:"",
      location:"",
      image:"",
      flag: false,
    }
  }
  handleOnChange = (event) => {
    const name = event.target.name;
    if(name === "image"){
      this.setState({
        [name]: event.target.files["0"].name,
      })
    }
    this.setState({
      [name]: event.target.value,
    })
  }

  onSubmit = (event) =>{
    event.preventDefault();
     this.setState({
       flag: true,
     })
  }
  render() {
    return (
      <div className="container-fluid mb-5" style={{ marginTop: "-5%" }}>
      <Modal flag={this.state.flag} />
        <h4 className="text-center">List Property here</h4>
        <div className="container mb-4 p-2 border">
          <form class="row g-3 needs-validation" novalidate onSubmit={this.onSubmit}>
            <div class="col-md-4">
              <label for="validationCustom01" class="form-label">
                Property name
              </label>
              <input
                type="text"
                name="name"
                class="form-control"
                id="validationCustom01"
                placeholder="Enter property name"
                onChange={this.handleOnChange}
                required
              />
              <div class="valid-feedback">Looks good!</div>
            </div>
            <div class="col-md-4">
              <label for="validationCustom02" class="form-label">
                Cost
              </label>
              <input
                type="text"
                name="cost"
                class="form-control"
                id="validationCustom02"
                placeholder="cost of property"
                onChange={this.handleOnChange}
                required
              />
              <div class="valid-feedback">Looks good!</div>
            </div>
            <div class="col-md-4">
              <label for="validationCustomUsername" class="form-label">
                Area
              </label>
              <div class="input-group has-validation">
                <input
                  type="text"
                  class="form-control"
                  name="area"
                  placeholder="Area of peroperty"
                  id="validationCustomUsername"
                  aria-describedby="inputGroupPrepend"
                  onChange={this.handleOnChange}
                  required
                />
                <div class="invalid-feedback">Please choose a username.</div>
              </div>
            </div>
            <div class="col-md-6">
              <label for="validationCustom03" class="form-label">
                Address
              </label>
              <input
                type="text"
                name="address"
                class="form-control"
                id="validationCustom03"
                placeholder="enter address"
                onChange={this.handleOnChange}
                required
              />
              <div class="invalid-feedback">Please provide a valid city.</div>
            </div>
            <div class="col-md-3">
              <label for="validationCustom04" class="form-label">
                Location
              </label>
              <select class="form-select" id="validationCustom04" name="location"  onChange={this.handleOnChange} required>
                <option selected disabled value="">
                 --select--
                </option>
                <option>Bangalore</option>
                <option>Chennai</option>
                <option>Pune</option>
                <option>Patna</option>
                <option>Mumbai</option>
              </select>
              <div class="invalid-feedback">Please select a valid state.</div>
            </div>
            <div class="col-md-3">
              <label for="validationCustom05" class="form-label">
                Images
              </label>
              <input
                type="file"
                name="image"
                class="form-control"
                id="validationCustom05"
                onChange={this.handleOnChange}
                required
              />
              <div class="invalid-feedback">Please provide a valid zip.</div>
            </div>
            <div class="col-12">
              <div class="form-check">
                <input
                  class="form-check-input"
                  type="checkbox"
                  value=""
                  id="invalidCheck"
                  required
                />
                <label class="form-check-label" for="invalidCheck">
                  Agree to terms and conditions
                </label>
                <div class="invalid-feedback">
                  You must agree before submitting.
                </div>
              </div>
            </div>
            <div class="col-12">
              <button class="btn btn-primary" type="submit">
                ADD
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
