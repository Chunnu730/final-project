import React from "react";
import Modal from "./Modal";
import icon from "../images/signup.png";
import validator from "validator";
import code from "../images/banner.jpg";
export default class Form extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state = {
      name: {
        value: "",
        valid: "",
        inValid: "",
        flag: false,
      },
      email: {
        value: "",
        valid: "",
        inValid: "",
        flag: false,
      },
      age: {
        value: "",
        valid: "",
        inValid: "",
        flag: false,
      },
      password: {
        value: "",
        valid: "",
        inValid: "",
        flag: false,
      },
      confirmPassword: {
        value: "",
        valid: "",
        inValid: "",
        flag: false,
      },
      terms: {
        checked: "",
        valid: false,
        inValid: "",
        flag: false,
      },
      modal: false,
    };
  }

  handleOnChange = (event) => {
    if (event.target.name === "name") {
      validator.isAlpha(event.target.value, "en-US", { ignore: " -" }) &
      validator.isLength(event.target.value, { min: 3, max: 25 })
        ? this.setState({
            name: {
              value: event.target.value,
              valid: "Looks good!",
              inValid: "",
              flag: true,
            },
            modal: false,
          })
        : this.setState({
            name: {
              value: event.target.value,
              valid: "",
              inValid:
                "Name should contains only alpha characters including space (' ') ranging from 3-25 characters",
            },
          });
    }

    if (event.target.name === "email") {
      validator.isEmail(event.target.value) &
      !/[A-Z$%*&#/?'~`=!+^(){}-]/g.test(event.target.value)
        ? this.setState({
            email: {
              value: event.target.value,
              valid: "Perfect!",
              inValid: "",
              flag: true,
            },
            modal: false,
          })
        : this.setState({
            email: {
              value: event.target.value,
              valid: "",
              inValid:
                "Your email address should contains only letters (a-z), numbers (0-9) and special characters (@._), format (example123@gmail.com)",
            },
          });
    }

    if (event.target.name === "age") {
      Number(event.target.value) >= 18 && Number(event.target.value) <= 60
        ? this.setState({
            age: {
              value: event.target.value,
              valid: "Perfect!",
              inValid: "",
              flag: true,
            },
            modal: false,
          })
        : this.setState({
            age: {
              value: event.target.value,
              valid: "",
              inValid: "Age must be ranging from 18-60",
            },
          });
    }

    if (event.target.name === "password") {
      validator.isStrongPassword(event.target.value)
        ? this.setState({
            password: {
              value: event.target.value,
              valid: "Strong password",
              inValid: "",
              flag: true,
            },
            modal: false,
          })
        : this.setState({
            password: {
              value: event.target.value,
              valid: "",
              inValid:
                "Password must be at least 8 characters that include at least 1 lowercase, 1 uppercase,1 number and 1 special character",
            },
          });
    }

    if (event.target.name === "confirmPassword") {
      (event.target.value === this.state.password.value) &
      (event.target.value !== "")
        ? this.setState({
            confirmPassword: {
              value: event.target.value,
              valid: "Password matches",
              inValid: "",
              flag: true,
            },
            modal: false,
          })
        : this.setState({
            confirmPassword: {
              value: event.target.value,
              valid: "",
              inValid: "Password is not matching",
            },
          });
    }

    if (event.target.name === "checkbox") {
      event.target.checked
        ? this.setState({
            terms: {
              valid: true,
              flag: true,
            },
            modal: false,
          })
        : this.setState({
            terms: {
              valid: false,
            },
          });
    }
  };

  onSubmitForm = (event) => {
    event.preventDefault();
    console.log("submit event");
    if ((this.state.name.value === "") & (this.state.name.inValid === "")) {
      this.setState({
        name: {
          inValid: "Please enter your name here",
        },
      });
    }

    if ((this.state.email.value === "") & (this.state.email.inValid === "")) {
      this.setState({
        email: {
          inValid: "Please enter your email id here",
        },
      });
    }

    if ((this.state.age.value === "") & (this.state.age.inValid === "")) {
      this.setState({
        age: {
          inValid: "Please enter your age here",
        },
      });
    }

    if (
      (this.state.password.value === "") &
      (this.state.password.inValid === "")
    ) {
      this.setState({
        password: {
          inValid: "Please set password here",
        },
      });
    }

    if (
      (this.state.confirmPassword.value === "") &
      (this.state.confirmPassword.inValid === "")
    ) {
      this.setState({
        confirmPassword: {
          inValid: "Please confirm password here",
        },
      });
    }

    if (this.state.terms.valid === false) {
      this.setState({
        terms: {
          inValid: " You must agree the terms and condition",
        },
      });
    }
    if (
      (this.state.name.flag === true) &
      (this.state.email.flag === true) &
      (this.state.age.flag === true) &
      (this.state.password.flag === true) &
      (this.state.confirmPassword.flag === true) &
      (this.state.terms.flag === true)
    ) {
      this.setState({
        modal: true,
      });
    }
    this.sendData();
  };
  sendData() {
    fetch("https://jsonplaceholder.typicode.com/posts").then((data) => {
      console.log(data);
    });
  }
  render() {
    return (
      <div className="form-container">
        <div className="container-box">
          <div className="container">
            <img src={code} alt="" className="code" />
          </div>
          <div className="container" style={{marginTop: "-15px"}}>
            <Modal flag={this.state.modal} />
            <div className="icon-box">
              <img src={icon} alt="sign icon" className="icon" />
            </div>
            <form>
              <div className="form-group row ">
                <div className="col">
                  <label>Name</label>
                  <input
                    type="text"
                    className="form-control "
                    required
                    name="name"
                    autoFocus
                    placeholder="Enter name"
                    defaultValue={this.state.name.value}
                    onChange={this.handleOnChange}
                  />
                  <div className="valid-data">{this.state.name.valid}</div>
                  <div className="invalid-data">{this.state.name.inValid}</div>
                </div>
              </div>

              <div className="form-group row ">
                <div className="col">
                  <label>Email</label>
                  <input
                    type="text"
                    className="form-control"
                    required
                    placeholder="Enter email"
                    name="email"
                    defaultValue={this.state.email.value}
                    onChange={this.handleOnChange}
                  />
                  <div className="valid-data">{this.state.email.valid}</div>
                  <div className="invalid-data">{this.state.email.inValid}</div>
                </div>
              </div>

              <div className="form-group  row ">
                <div className="col">
                  <label>Age</label>
                  <input
                    type="number"
                    className="form-control"
                    required
                    placeholder="Enter age"
                    name="age"
                    defaultValue={this.state.age.value}
                    onChange={this.handleOnChange}
                  />
                  <div className="valid-data">{this.state.age.valid}</div>
                  <div className="invalid-data">{this.state.age.inValid}</div>
                </div>
              </div>

              <div className="form-group row ">
                <div className="col">
                  <label>Password</label>
                  <input
                    type="password"
                    className="form-control"
                    required
                    placeholder="Enter password"
                    name="password"
                    defaultValue={this.state.password.value}
                    onChange={this.handleOnChange}
                  />
                  <div className="valid-data">{this.state.password.valid}</div>
                  <div className="invalid-data">
                    {this.state.password.inValid}
                  </div>
                </div>
              </div>

              <div className="form-group  row ">
                <div className="col">
                  <label>Confirm Password</label>
                  <input
                    type="password"
                    className="form-control "
                    name="confirmPassword"
                    required
                    placeholder="Confirm password"
                    defaultValue={this.state.confirmPassword.value}
                    onChange={this.handleOnChange}
                  />
                  <div className="valid-data">
                    {this.state.confirmPassword.valid}
                  </div>
                  <div className="invalid-data">
                    {this.state.confirmPassword.inValid}
                  </div>
                </div>
              </div>

              <div className="form-group  row m-1 ">
                <div class="form-check  col">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="invalidCheck"
                    required
                    name="checkbox"
                    onChange={this.handleOnChange}
                  />
                  <label class="form-check-label mt-1" for="invalidCheck">
                    Agree to terms and conditions
                  </label>
                  <div class="invalid-data">{this.state.terms.inValid}</div>
                </div>
              </div>
              <div className="form-group row align-items-center">
                <div class="col-12">
                  <input
                    class="btn btn-primary"
                    type="submit"
                    style={{ width: "100%" }}
                    value={`Sign-up`}
                    onClick={this.onSubmitForm}
                  />
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
