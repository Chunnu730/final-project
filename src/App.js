import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./components/Home";
import Properties from "./components/Properties";
import Details from "./components/Details";
import Form from "./components/Form";
import List from "./components/List";
import Show from "./components/Show";

export default class App extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/properties/:location" component={Properties} />
          <Route exact path="/properties/details/:location" component={Details} />
          <Route exact path="/signup" component={Form} />
          <Route exact path="/list" component={List}/>
          <Route exact path="/show" component={Show} />
        </Switch>
      </Router>
    );
  }
}
